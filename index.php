<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Générateur de requêtes SQL pour changer le nom de domaine d'un site WordPress</title>
        <meta name="description" content="Générer les commandes SQL nécessaires au changement d'URL d'un site WordPress">
        <meta name="Author" content="Clément Biron" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://cdn.jsdelivr.net/npm/vue"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="src/css/prism.css">
        <script src="src/js/prism.js"></script>
        <script src="src/js/main.js"></script>
        <link rel="stylesheet" href="src/css/main.css">
        

            <!-- Piwik -->
            <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                var u="//stats.clementbiron.com/";
                _paq.push(['setTrackerUrl', u+'piwik.php']);
                _paq.push(['setSiteId', '5']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
            })();
            </script>
            <!-- End Piwik Code -->

    </head>
    <body>

        <section class="hero is-dark is-bold">
            <div class="hero-body">
                <div class="container">
                    <h1 class="title">
                        Changer le nom de domaine d'un site WordPress
                    </h1>
                    <h2 class="subtitle">
                        Générateur de commandes SQL pour changer l'URL du site dans la base de données
                    </h2>
                </div>
            </div>
        </section>

        <div id="app">
            <div class="section">
                <div class="form container">
                    <h3 class="subtitle is-3">Configuration</h3>
                    <div class="columns">      

                        <div class="column">
                            <div class="field">
                                <label class="label" for="input_prefixe">Prefixe des tables</label>
                                <div class="control">
                                    <input class="input" type="text" :placeholder="prefixe" name="input_prefixe" v-model="prefixe">
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="field">
                                <label class="label" for="input_previousurl">Ancienne URL</label>
                                <div class="control">
                                    <input class="input" type="text" :placeholder="previousURL" name="input_previousurl" v-model="previousURL">
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="field">
                                <label class="label" for="input_nexturl">Prochaine URL</label>
                                <div class="control">
                                    <input class="input" type="text" :placeholder="nextURL" name="input_nexturl" v-model="nextURL">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="section">
                <div class="generated container">
                    <h3 class="subtitle is-3">Commandes a éxecuter</h3>
                    <pre>
                        <code class="language-sql"> 
                            UPDATE `{{ prefixe }}options` SET option_value = replace(option_value, '{{ previousURL }}', '{{ nextURL }}') WHERE option_name = 'home' OR option_name = 'siteurl'; 
                            UPDATE `{{ prefixe }}posts` SET guid = replace(guid, '{{ previousURL }}','{{ nextURL }}'); 
                            UPDATE `{{ prefixe }}posts` SET post_content = replace(post_content, '{{ previousURL }}', '{{ nextURL }}'); 
                            UPDATE `{{ prefixe }}postmeta` SET meta_value = replace(meta_value,'{{ previousURL }}','{{ nextURL }}'); 
                            UPDATE `{{ prefixe }}links` SET link_url = replace(link_url,'{{ previousURL }}', '{{ nextURL }}');
                        </code>
                    </pre>
                </div>  
            </div>  
        </div>

        <div class="section">
            <div class="container">
                <h3 class="subtitle is-3">A propos</h3>
                <p></p>
                <article class="message is-warning ">
                    <div class="message-body">
                        <p>Ces commandes ne sont pas parfaites, notamment parcequ'elles ne prennent pas en compte les données sérialisées. Il existe des outils plus élaborés (voir ci-dessous). Pensez à toujours faire une sauvegarde prélimilaire de votre base de données 
                        <span class="icon has-text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></span><strong> et si vous ne savez pas ce que vous faites, ne le faites pas ! </strong><span class="icon has-text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></span></p>
                        <p>Vous pouvez demander un coup de main sur : <a href="http://wordpressfr.slack.com/">Slack WordPress Fr</a> ou <a href="https://wpfr.net/support/">le support de l'association WPFR</a> ou encore <a href="https://www.facebook.com/groups/WordPressAcademy/">le groupe Facebook WordPress Academy</a>.</p>
                    </div>
                </article>
                <div class="columns">
                <div class="column">
                        <h3 class="subtitle is-3">Quelques outils</h3>
                        <article class="message is-success">
                            <div class="message-body">
                                <p class="has-text-info"><strong>L'extension que je recommande est <a href="http://t.deliciousbrains.com/SH6V">WP Migrate DB Pro</a></strong> (payante, lien affilié), qui permet en un clic de pousser / récupérer des bases de données WordPress.</p>
                                <p>Il y a aussi <a href="https://wordpress.org/plugins/duplicator/">Duplicator</a> et <a href="https://wordpress.org/plugins/all-in-one-wp-migration/">All-in-One WP Migration</a> (gratuites, mais je n'ai pas testé).</p>
                                <p>Enfin les incontournables <a href="http://wp-cli.org/">WP CLI</a> avec la commande `search-replace` et le script PHP <a href="https://github.com/interconnectit/Search-Replace-DB">SRBD</a>.</p>
                            </div>
                        </article>
                    </div>
                    <div class="column">
                        <h3 class="subtitle is-3">Un peu de lecture</h3>
                        <article class="message is-dark">
                            <div class="message-body">
                                <p><ul>
                                    <li><a href="https://wpmarmite.com/migrer-wordpress-manuellement/">Migrer un site WordPress manuellement</a>, par Aurélien Debord  sur WP marmite</li>
                                    <li><a href="https://www.gregoirenoyelle.com/wordpress-migrer-son-site-local-vers-le-serveur-en-ligne/">Migrer son site du local vers le serveur en ligne</a>, par et chez Gregoire Noyelle</li>
                                    <li><a href="https://korben.info/changer-le-nom-de-domaine-dun-blog-wordpress-sans-encombres.html">Changer le nom de domaine d’un blog WordPress sans encombres</a>, par et chez Korben</li>
                                    <li><a href="https://wpformation.com/deplacer-wordpress-nom-domaine/">Déplacer WordPress vers un nouveau Nom de Domaine</a>, par Fabrice Ducarme chez WP Formation</li>
                                    <li><a href="https://www.maximeculea.fr/astuce-comment-changer-les-urls-dune-bdd-avec-dbsr/">Changer les urls d’une BDD avec DBSR</a>, par et chez Maxime Culea</li>
                                </ul></p>
                            </div>
                        </article>
                    </div>
                    
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="content has-text-centered">
                    <p>
                        <strong><a href="http://clementbiron.com">Clément Biron</a></strong><br>
                        <a href="https://twitter.com/clementbiron"><span class="icon has-text-grey-dark"><i class="fa fa-twitter-square" aria-hidden="true"></i></span></a>
                        <a href="https://www.linkedin.com/in/clementbiron/"><span class="icon has-text-grey-dark"><i class="fa fa-linkedin-square" aria-hidden="true"></i></span></a>
                        <a href="https://github.com/posykrat"><span class="icon has-text-grey-dark"><i class="fa fa-github-square" aria-hidden="true"></i></span></a>
                        <a href="http://la-binocle.com/"><span class="icon has-text-grey-dark"><i class="fa fa-camera" aria-hidden="true"></i></span></a>
                    </p>
                </div>
            </div>
        </footer>
    </body>
</html>